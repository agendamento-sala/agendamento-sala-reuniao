'use strict'

const repository = require('../repositories/sala-repository');
const ctrlBase = require('../bin/base/controller-base');
const validation = require('../bin/helpers/validation');
const _repo = new repository();

function salaController() {

}

salaController.prototype.post = async (req, res) => {

    let _validationContract = new validation();
    /*_validationContract.isRequired(req.body.titulo, 'o título é obrigatório');
    _validationContract.isRequired(req.body.foto, 'A foto é obrigatória'); */

    ctrlBase.post(_repo, _validationContract, req, res);
    
};

salaController.prototype.put = async (req, res) => {

    let _validationContract = new validation();
    /*_validationContract.isRequired(req.body.titulo, 'o título é obrigatório');
    _validationContract.isRequired(req.body.foto, 'A foto é obrigatória');
    _validationContract.isRequired(req.params.id, 'O Id que será atualizado é obrigatório');*/

    ctrlBase.put(_repo, _validationContract, req, res);
};

salaController.prototype.get = async (req, res) => {
    ctrlBase.get(_repo, req, res);
};

salaController.prototype.getById = async (req, res) => {
    ctrlBase.getById(_repo, req, res);
};

salaController.prototype.delete = async (req, res) => {
    ctrlBase.delete(_repo, req, res);
};

module.exports = salaController;