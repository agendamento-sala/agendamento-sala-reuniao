const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
const salaRouter = require('../routes/sala-router');


// swagger definition
var swaggerDefinition = {
  info: {
    title: 'Node Swagger API',
    version: '1.0.0',
    description: 'Demonstrating how to describe a RESTful API with Swagger',
  },
  host: 'localhost:3000',
  basePath: '/',
  securityDefinitions: {
    "Bearer": {
      "name": "Authorization",
      "in": "header",
      "type": "apiKey"
    }
  },
  "security": [
    {
      "Bearer": []
    }
  ]
};


// options for the swagger docs
var options = {
  // import swaggerDefinitions
  swaggerDefinition: swaggerDefinition,
  // path to the API docs
  //apis: ['../routes/*.js'],
  apis: ['./*.js'],
};
 
const swaggerSpec = swaggerJSDoc(options);




mongoose.connect(process.env.MONGODB_URI, { useNewUrlParser: true })

const Agenda = mongoose.model('Agenda', new mongoose.Schema({
  data: Date,
  organizador: String,
  titulo: String,
  descricao: String,
  duracao: Number
}))

const api = express()
.use(bodyParser.json())

/**
 * @swagger
 * /api/agenda:
 *   get:
 *     tags:
 *       - Agenda
 *     description: Listar
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of puppies
 *     parameters:
 *       - in: query
 *         name: teste
 *         schema:
 *           type: string
 *         required: true
 *         description: teste apenas
 *   post:
 *     summary: Adiciona um registro na agenda
 *     requestBody:
 *       content:
 *         application/json:
 *           schema:      # Request body contents
 *             type: object
 *             properties:
 *               organizador:
 *                 type: string
 *               titulo:
 *                 type: string
 *             example:   # Sample object
 *               organizador: organizador
 *               titulo: titulo teste 
 */



.get('/agenda', (req, res) => Agenda.find({}, (err, agendas) => res.status(200).send(agendas)))
.post('/agenda', (req, res) => {
  let agenda = new Agenda

  agenda.data = req.body.data
  agenda.organizador = req.body.organizador
  agenda.titulo = req.body.titulo
  agenda.descricao = req.body.descricao
  agenda.duracao = req.body.duracao

  agenda.save((err) => {
    if(!err) {
      res.status(201)
      res.send(req.body)
    }
    else {
      res.status(400)
      res.send(err)
    }
  })
})

const app = express()
.use(require('express').static('public'))
.use('/api', api)
.use('/api/sala', salaRouter)

// serve swagger
app.get('/swagger.json', function(req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.send(swaggerSpec);
});

 
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));


//Exportando nossa Api
module.exports = app;
