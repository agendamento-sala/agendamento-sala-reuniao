const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const configuration = require('../bin/configuration/variables');
const cors = require('cors');
const moment = require('moment')


mongoose.connect(configuration.Database.connection, { useCreateIndex: true, useNewUrlParser: true })

const api = express()
    .use(bodyParser.json())
    .use(cors());

// rotas
const salaRouter = require('../routes/sala-router');

api.use(require('express').static('public'));

api.use('/api/sala', salaRouter);


const Agenda = mongoose.model('Agenda', new mongoose.Schema({
    titulo: String,
    dataHoraInicio: Date,
    dataHoraFim: Date,
    sala: String,
    organizador: String
}));


api.get('/api/agenda', (req, res) => {
    let start = req.query.dataInicial == "" ? undefined : req.query.dataInicial;
    let end = req.query.dataFinal == "" ? undefined : req.query.dataFinal;
    let sala = req.query.sala == "" ? undefined : req.query.sala;
    let ordem = { sala: 1, dataHoraInicio: 1 };

    let query = {
        $and: [
            { $or: [{ undefined: { $eq: sala } }, { sala: { $eq: sala } }] },
            { $or: [{ undefined: { $eq: start } }, { dataHoraInicio: { $gte: start } }] },
            { $or: [{ undefined: { $eq: end } }, { dataHoraInicio: { $lt: end } }] }
        ]
    };

    Agenda.find(query, (err, agendas) => res.status(200).send(agendas)).sort(ordem);

});

api.get('/api/agenda/:id', (req, res) => {
    Agenda.findOne({ '_id': Object(req.params.id) }, (err, agenda) => res.status(200).send(agenda));
});


function validar(pAgenda) {

    v_mensagem = "";

    if (pAgenda.dataHoraInicio == null) {
        v_mensagem = '[dataHoraInicio] deve ser preenchida!';
    }
    else if (pAgenda.dataHoraFim == null) {
        v_mensagem = '[dataHoraFim] deve ser preenchida!';
    }
    else if (pAgenda.titulo == null) {
        v_mensagem = '[titulo] deve ser preenchido!';
    }
    else if (pAgenda.organizador == null) {
        v_mensagem = '[organizador] deve ser preenchida!';
    }
    else if (pAgenda.sala == null) {
        v_mensagem = '[sala] deve ser preenchida!';
    }

    return v_mensagem;
}

async function AgendaExiste(pData) {
    let pInicio = pData.dataHoraInicio;
    let pFim = pData.dataHoraFim;
    let pSala = pData.sala;
    let pId = pData._id;

    v_registro = await Agenda.
        findOne( { sala: pSala,
                   _id: { $ne: pId }
                } ).and([
                    { $or: [{ dataHoraInicio: { $gte: pInicio, $lte: pFim }},
                            { dataHoraFim: { $gte: pInicio, $lte: pFim }}, {
                            $and: [ { dataHoraInicio: { $lte: pInicio } },
                                    { dataHoraFim: { $gte: pFim } }] }
                    ]}
                ]);

    return v_registro != null;
}


async function gravar(req, res) {

    let data = req.body;
    let modelo = new Agenda(data);

    v_mensagem = validar(modelo);

    if (v_mensagem == "") {
        let v_registro = await AgendaExiste(data);

        if (v_registro) {
            v_mensagem = 'Já existe uma agenda para a Sala : "' + v_registro.sala + '" no período entre ' + v_registro.dataHoraInicio + " `as " + v_registro.dataHoraFim;
            res.status(400).send({ message: v_mensagem });
        }
        else {
            modelo.save(function (err, agenda) {
                if (err) {
                    res.status(500).send({ message: 'Erro no processamento', error: err });
                }
                else {
                    res.status(201).send(agenda);
                }
            });
        }
    }
    else {
        res.status(400).send({ message: v_mensagem });
    }
}


api.post('/api/agenda', gravar);

api.delete('/api/agenda/:id', (req, res) => {
    Agenda.deleteOne({ '_id': Object(req.params.id) }, (err, agenda) => res.status(200).send(agenda));
});

api.put('/api/agenda', atualizar);

async function atualizar(req, res) {
    let data = req.body;
    var id = req.body._id;

    let v_qtd_registro = await AgendaExiste(data);
    if (v_qtd_registro) {
        res.status(400).send({ message: 'Existe uma agenda para esta Sala entre o período informado.' });
    }
    else {
        Agenda.findOne({ '_id': Object(id) }, function (err, agenda) {
            if (err) {
                res.status(500).send({ message: 'Erro no processamento.', error: err });
            }

            agenda.sala = data.sala;
            agenda.dataHoraInicio = data.dataHoraInicio;
            agenda.dataHoraFim = data.dataHoraFim;
            agenda.titulo = data.titulo;
            agenda.organizador = data.organizador;
            agenda.save(function (err, agenda) {
                if (err) {
                    res.status(500).send({ message: 'Erro na gravação', error: err });
                }
                else {
                    res.status(201).send(agenda);
                }
            });
            return;
        });
    }
};


//Exportando nossa Api
module.exports = api;
