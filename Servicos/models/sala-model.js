'use strict'

const mongoose = require('mongoose');
const schema = mongoose.Schema;

const salaModel = new schema({
    nome: { trim: true, index: true, required: true, type: String },
    dataCriacao: { type: Date, default: Date.now }
}, { versionKey: false });

salaModel.pre('save', next => {
    let agora = new Date();
    if (!this.dataCriacao)
        this.dataCriacao = agora;
    next();
});

module.exports = mongoose.model('Sala', salaModel);