'use strict'

const mongoose = require('mongoose');
const schema = mongoose.Schema;

const agendaModel = new schema({
     
    _id: String,  
    titulo: String,
    dataHoraInicio: Date,
    dataHoraFim: Date,
    sala: String,
    organizador: String,
    dataCriacao: { type: Date, default: Date.now }
}, { versionKey: false });

agendaModel.pre('save', next => {
    let agora = new Date();
    if (!this.dataCriacao)
        this.dataCriacao = agora;
    next();
});

module.exports = mongoose.model('Agenda', agendaModel);


/* 'use strict'

const mongoose = require('mongoose');
const schema = mongoose.Schema;

const agendaModel = new schema({
    data: Date,
    organizador: String,
    titulo: String,
    descricao: String,
    duracao: Number,
    sala: String
}, { versionKey: false });

module.exports = mongoose.model('Agenda', agendaModel);

*/