# Variáveis de ambiente
* `MONGODB_URI=mongodb+srv://maia:1Q2w3e4r@freela-kc0mt.azure.mongodb.net/test?retryWrites=true`
* `PORT=3000`


# Como compilar o projeto
`npm install`


# Como rodar o projeto
`node app.js`


# Posso usar o `nodemon`?
Sim


# Endpoints para teste do back-end:

## * Salvar nova agenda
```
curl -X POST \
  http://localhost:3000/api/agenda \
  -H 'Content-Type: application/json' \
  -d '{
  "data": "Tue May 29 2019 13:30:00 GMT-0300",
  "organizador": "Maia",
  "titulo": "Primeira reuniao",
  "descricao": "Aprentação do primeiro aplicativo.",
  "duracao": 20
}'
```

## * Ver todas as agendas
```
curl -X GET http://localhost:3000/api/agenda
```
