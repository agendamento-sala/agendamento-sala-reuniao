'use strict'

const app = require('./bin/mainApp');
const configuration = require('./bin/configuration/variables');




app.get('*', (req, res) => {
  res.sendFile(require('path').join(__dirname, 'dist/index.html'));
});

//const server = app.listen(process.env.PORT, () =>
//  console.log(`Aplicação iniciada na porta ${server.address().port}...`)
//)

 
 

const server = app.listen(configuration.Api.port, () =>
  console.log(`Aplicação iniciada na porta ${server.address().port}...`)
)
