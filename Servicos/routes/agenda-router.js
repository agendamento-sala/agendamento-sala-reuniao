'use strict'

const express = require('express');
const router = express.Router();
const controller = require('../controllers/agenda-controller');
//const auth = require('../middlewares/authenctication');

const configuration = require('../bin/configuration/variables');

const mongoose = require('mongoose');


mongoose.connect(configuration.Database.connection, { useNewUrlParser: true })


console.log("teste");

//module.exports = //
const Agenda = mongoose.model('Agenda2', new mongoose.Schema({
    titulo: String,
    dataInicio: Date,
    dataFim: Date,
    sala: String,
    organizador: String
  }, { versionKey: false }));


let _ctrl = new controller();

//const listar = (req, res) => Agenda.find(req.query, (err, agendas) => res.send(agendas))
/*if(!err) 
{
    console.log("erro de execucao" & err)
}
*/

 

const getByName = (req, res) => {

    console.log("getbyname");

    // apenas para testar o loading 
    const sleep = (waitTimeInMs) => new Promise(resolve => setTimeout(resolve, waitTimeInMs));
    
    sleep(10000).then(() => {
        // This will execute 10 seconds from now
     

    try {       
        if (req.query) {
            Agenda.find(req.query,  (err, agendas) => res.send(agendas))
        } else {
            res.status(404).send({ message: 'O parametro nome da sala precisa ser informado.' });
        }
    } catch (error) {
        console.log('getByName com error, motivo: ', err);
        res.status(500).send({ message: 'Erro no processamento', error: err });
    } 

    });
}; 

//router.get('/', auth, _ctrl.get);
//router.get('/:id', auth, _ctrl.getById);
//router.post('/', auth, _ctrl.post);
//router.put('/:id', auth, _ctrl.put);
//router.delete('/:id', auth, _ctrl.delete);

//router.get('/', _ctrl.get);


router.get('/', getByName);
//router.get('/', listar);
router.get('/:id', _ctrl.getById);
router.post('/', _ctrl.post);
router.put('/:id', _ctrl.put);
router.delete('/:id', _ctrl.delete);

module.exports = router;

