import React, { Component } from 'react';
import { View, Text, FlatList,  StyleSheet } from 'react-native';
import api from '../services/api';
import Moment from 'moment';

export default class Main extends Component {
    static navigationOptions = {
        title: "Agendamento"
    }

    state = {
        dados: []
    };

    componentDidMount() {
        this.carregarAgendamentos();
    }

    carregarAgendamentos = async () => {
        const response = await api.get('/agenda');

        this.setState( { dados: response.data } );
    }

    renderItem = ({item}) => (
        <View style={styles.agendaContainer}>
            <Text>{item.sala}</Text>
            <Text>{Moment(item.dataHoraInicio).format('DD/MM/YYYY HH:mm')}</Text>
            <Text>{Moment(item.dataHoraFim).format('DD/MM/YYYY HH:mm')}</Text>
            <Text>{item.titulo}</Text>
            <Text>{item.organizador}</Text>
        </View>
    )

    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    data={this.state.dados}
                    keyExtractor={item => item._id}
                    renderItem={this.renderItem}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#000'
    },
    agendaContainer: {
      borderRadius: 4,
      borderWidth: 0.5,
      borderColor: 'silver',
      marginBottom: 10,
      backgroundColor: 'white',
      padding: 15
    },
    title: {
      fontSize: 19,
      fontWeight: 'bold',
    }
  });