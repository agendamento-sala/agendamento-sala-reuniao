export class Agenda {
    _id: string;
    sala: string;
    titulo: string;
    dataHoraInicio: Date;
    dataHoraFim: Date;    
    organizador: string;
}
