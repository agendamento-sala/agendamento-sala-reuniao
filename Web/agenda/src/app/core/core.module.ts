import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HeaderComponent } from './header/header.component';
import { ErrosModule } from './erros/erros.module';
import { AlertModule } from '../shared/components/alert/alert.module';
import { SobreComponent } from './sobre/sobre.component';

@NgModule({
  declarations: [
    HeaderComponent,
    SobreComponent
  ],
  imports: [
    CommonModule,
    ErrosModule,
    AlertModule
  ],
  exports: [
    HeaderComponent,
    SobreComponent
  ]
})
export class CoreModule {

}
