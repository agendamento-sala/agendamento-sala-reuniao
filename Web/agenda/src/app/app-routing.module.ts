import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { AgendamentoListaComponent } from './agendamento/agendamento-lista/agendamento-lista.component';
import { AgendamentoFormComponent } from './agendamento/agendamento-form/agendamento-form.component';
import { NotFoundComponent } from './core/erros/not-found/not-found.component';
import { SobreComponent } from './core/sobre/sobre.component';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'agenda'
    },
    {
        path: 'agenda',
        component:
        AgendamentoListaComponent
    },
    {
        path: 'sobre',
        component:
        SobreComponent
    },
    {
        path: 'agenda/:id',
        component: AgendamentoFormComponent
    },
    {
        path: 'not-found',
        component: NotFoundComponent,
    },
    {
        path: '**',
        redirectTo: 'not-found'
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forRoot(routes)
    ],
    exports: [RouterModule],
    declarations: []
})
export class AppRoutingModule { }
