import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';

import { Agenda } from '../../models/agenda';
import { AgendaService } from '../agenda-service';
import { AlertService } from 'src/app/shared/components/alert/alert.service';


@Component({
    selector: 'app-agendamento-form',
    templateUrl: './agendamento-form.component.html',
    styleUrls: ['./agendamento-form.component.css']
})
export class AgendamentoFormComponent implements OnInit {

    agendaForm: FormGroup;
    agenda: Agenda;
    agendaId: string;

    submitted: boolean = false;

    constructor(
        private fb: FormBuilder,
        private agendaService: AgendaService,
        private route: ActivatedRoute,
        private alertService: AlertService,
        private router: Router) { }

    ngOnInit() {
        this.agenda = new Agenda();
        this.agendaForm = this.createGroup();
        this.carregarAgenda();
    }

    carregarAgenda() {
        this.agendaId = this.route.snapshot.params.id;
        if (this.agendaId.length <= 0) return;

        this.agendaService
            .buscarAgenda(this.agendaId)
            .subscribe(
                agenda => {
                    this.agenda = agenda;
                    if (this.agendaId && !this.agenda) {
                        this.alertService.danger("Agenda não Existe", true);
                        this.router.navigate(['/agenda']);
                    }
                },
                err => {
                    console.log(err);
                    this.alertService.danger("Erro na conexão");
                }
            );
    }

    private createGroup(): FormGroup {
        return this.fb.group({
            _id: [''],
            sala: ['', Validators.required],
            dataHoraInicio: ['', Validators.required],
            dataHoraFim: ['', Validators.required],
            tituloAgenda: ['', Validators.required],
            organizador: ['', Validators.required]
        });
    }

    get f() {
        return this.agendaForm.controls;
    }

    onVoltar() {
        this.router.navigate(['/agenda']);
    }

    validacoesData(): boolean {
        let hoje = moment(new Date()).format("YYYYMMDDHHmm");
        let dtIni = moment(this.agenda.dataHoraInicio).format("YYYYMMDDHHmm");
        let dtFim = moment(this.agenda.dataHoraFim).format("YYYYMMDDHHmm");
        let diaMesAnoIni = moment(this.agenda.dataHoraInicio).format("YYYYMMDD");
        let diaMesAnoFin = moment(this.agenda.dataHoraFim).format("YYYYMMDD");
        let hrMinIni = moment(this.agenda.dataHoraInicio).format("HHmm");
        let hrMinFin = moment(this.agenda.dataHoraFim).format("HHmm");

        if (dtIni <= hoje) {
            this.alertService.danger("Data/Hora Inicial não pode ser maior ou igual a Data/Hora Hoje.");
            return false;
        }
        if (dtIni >= dtFim) {
            this.alertService.danger("Data/Hora Inicial não pode ser maior ou igual a Final.");
            return false;
        }
        if (diaMesAnoIni == diaMesAnoFin) {
            if ((Number(this.agenda.dataHoraFim) / 1000) - (Number(this.agenda.dataHoraInicio) / 1000) < 1807) {
                this.alertService.danger("O tempo mínimo de agenda é de 30 minutos.");
                return false;
            }
        }
        if (hrMinIni < "0700") {
            this.alertService.danger("Hora Inicial tem que ser maior ou igual a 07:00h.");
            return false;
        }
        if (hrMinIni > "2230") {
            this.alertService.danger("Hora Inicial tem que ser menor ou igual a 22:30h.");
            return false;
        }
        if (hrMinFin < "0730") {
            this.alertService.danger("Hora Final tem que ser maior ou igual a 07:30h.");
            return false;
        }
        if (hrMinFin > "2300") {
            this.alertService.danger("Hora Final tem que ser menor ou igual a 23:00h.");
            return false;
        }
        return true;
    }

    salvarAgenda() {
        this.submitted = true;

        if ((!this.agendaForm.valid) && (!this.agendaForm.pending)) return;

        if (!this.validacoesData()) return;

        if (!this.agenda._id) {
            this.incluirAgenda();
        }
        else {
            this.alterarAgenda();
        }
    }

    incluirAgenda() {

        this.agendaService
            .incluirAgenda(this.agenda)
            .subscribe(
                () => {
                    this.alertService.danger("Incluído o registro.");
                    this.router.navigate(['/agenda']);
                },
                erro => {
                    console.log(erro);
                    this.alertService.danger(erro.error.message);
                }
            );
    }

    alterarAgenda() {

        this.agendaService
            .alterarAgenda(this.agenda)
            .subscribe(
                () => {
                    this.alertService.danger("Alterado o registro.");
                    this.router.navigate(['/agenda']);
                },
                erro => {
                    console.log(erro);
                    this.alertService.danger(erro.error.message);
                }
            );
    }

}
