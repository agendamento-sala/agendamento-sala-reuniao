import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationService } from 'primeng/api';
import * as moment from 'moment';

import { Agenda } from 'src/app/models/agenda';
import { AgendaFiltro } from 'src/app/filtros/agendaFiltro';
import { AgendaService } from '../agenda-service';
import { AlertService } from '../../shared/components/alert/alert.service';


@Component({
    selector: 'app-agendamento-lista',
    templateUrl: './agendamento-lista.component.html'
})
export class AgendamentoListaComponent implements OnInit {

    agendas: Agenda[];

    vAgendaFiltro = new AgendaFiltro();

    processando_pesquisa = false;
    error_message = "";

    constructor(
        private router: Router,
        private servico: AgendaService,
        private alertService: AlertService,
        private confirmationService: ConfirmationService) {
    }

    ngOnInit() {

        this.pesquisar();
    }

    onIncluir() {
        this.router.navigate(['/agenda', '']);
    }

    onAlterar(id: string) {
        this.router.navigate(['/agenda', id]);
    }

    onExcluir(id: string) {

        this.servico
            .buscarAgenda(id)
            .subscribe(
                agenda => {
                    NgbDateParserFormatter
                    let msgConfirmacao = "Tem certeza que deseja excluir esse registro de:" +
                                         "<br>Sala: " + agenda.sala + "<br>Data/Hora: " + moment(agenda.dataHoraInicio).format("DD/MM/YYYY HH:mm") + " ?";

                    this.confirmationService.confirm({
                        message: msgConfirmacao,
                        rejectLabel: "Não",
                        acceptLabel: "Sim",
                        accept: () => {
                            this.servico
                                .excluirAgenda(id)
                                .subscribe(
                                    (agendas) => {
                                        this.alertService.sucess('Exclusão de registro.', true);
                                        this.pesquisar();
                                    },
                                    erro => {
                                        console.log(erro);
                                        this.alertService.danger('Erro na exclusão.', true);
                                    }
                                );
                        }
                    });
                },
                erro => {
                    this.alertService.danger('Erro na carga da informação.', true);
                }
            );
    }

    pesquisar() {

        if ( (this.vAgendaFiltro.dataInicio != undefined) && (this.vAgendaFiltro.dataFim != undefined) )
        {
            if ( moment(this.vAgendaFiltro.dataInicio).format("YYYYMMDD") > moment(this.vAgendaFiltro.dataFim).format("YYYYMMDD") )
            {
                this.alertService.danger('Data Inicial está maior que a Data Final.', true);
                return;
            }
        }

        this.servico
            .listaAgenda(this.vAgendaFiltro)
            .subscribe(
                agendas => {
                    this.agendas = agendas;
                },
                err => {
                    console.log(err);
                    this.alertService.danger('Erro na conexão.', true);
                }
            );
    }

    confirm() {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            accept: () => {
                //Actual logic to perform a confirmation
            }
        });
    }
}
