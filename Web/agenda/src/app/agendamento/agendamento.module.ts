import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { CalendarModule } from 'primeng/calendar';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AgendamentoListaComponent } from './agendamento-lista/agendamento-lista.component';
import { AgendamentoFormComponent } from './agendamento-form/agendamento-form.component';

import { DateTimeAdapter, OwlDateTimeModule, OwlNativeDateTimeModule, OWL_DATE_TIME_LOCALE, OwlDateTimeIntl} from 'ng-pick-datetime';
import { NativeDateTimeAdapter } from 'ng-pick-datetime/date-time/adapter/native-date-time-adapter.class';
import { Platform } from '@angular/cdk/platform';

export class BrazilIntl extends OwlDateTimeIntl {

    /** A label for the cancel button */
    cancelBtnLabel = 'Cancelar';

    /** A label for the set button */
    setBtnLabel = 'Selecionar';

    /** A label for the range 'from' in picker info */
    rangeFromLabel = 'De';

    /** A label for the range 'to' in picker info */
    rangeToLabel = '';

    /** A label for the hour12 button (AM) */
    hour12AMLabel = 'AM';

    /** A label for the hour12 button (PM) */
    hour12PMLabel = 'PM';
}



@NgModule({
    declarations: [
        AgendamentoListaComponent,
        AgendamentoFormComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        CalendarModule,
        HttpClientModule,
        ConfirmDialogModule,
        NgbModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule
    ],
    exports: [
        AgendamentoListaComponent,
        AgendamentoFormComponent
    ],
    providers: [
        ConfirmationService,
        // The locale would typically be provided on the root module of your application. We do it at
        // the component level here, due to limitations of our example generation script.
         {provide: OWL_DATE_TIME_LOCALE, useValue: 'pt-BR'},
         {provide: DateTimeAdapter, useClass: NativeDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE, Platform]},
         {provide: OwlDateTimeIntl, useClass: BrazilIntl},

    ],
})
export class AgendamentoModule {

}

