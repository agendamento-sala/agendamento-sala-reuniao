import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import * as moment from 'moment';

import { Agenda } from '../models/agenda';
import { AgendaFiltro } from '../filtros/agendaFiltro';


@Injectable({ providedIn: 'root' }) // qualquer componente pode utilizar este service
export class AgendaService {

    private agendaUrl = 'http://localhost:3000/api/agenda';

    constructor(private httpClient: HttpClient) { }

    listaAgenda(pAgendaFiltro : AgendaFiltro) {
        let dtInicial = pAgendaFiltro.dataInicio != undefined ? moment(pAgendaFiltro.dataInicio).format() : "";
        let dtFinal = pAgendaFiltro.dataFim != undefined ? moment(pAgendaFiltro.dataFim).add(24, 'hours').format() : "";

        const  params = new HttpParams()
            .set('sala', pAgendaFiltro.sala)
            .set("dataInicial", dtInicial )
            .set("dataFinal", dtFinal);

        return this.httpClient
            .get<Agenda[]>(this.agendaUrl, { params });
    }

    buscarAgenda(id: String) {
        return this.httpClient
            .get<Agenda>(`${this.agendaUrl}/${id}`);
    }

    excluirAgenda(id: String) {
        return this.httpClient.delete(`${this.agendaUrl}/${id}`);
    }

    incluirAgenda(agenda: Agenda) {
        return this.httpClient.post(this.agendaUrl, agenda);
    }

    alterarAgenda(agenda: Agenda) {
        return this.httpClient.put(this.agendaUrl, agenda);
    }

}
